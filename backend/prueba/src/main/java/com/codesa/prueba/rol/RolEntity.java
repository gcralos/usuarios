package com.codesa.prueba.rol;

import com.codesa.prueba.usuario.UsuarioEntity;

import javax.persistence.*;
import java.util.List;

@Entity
public class RolEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO )
    private long  rolId;

    @Column
    private String nombre;

    @OneToMany(mappedBy = "rolEntity")
    private List<UsuarioEntity>  usuarioEntities;

    public void setRolId(long rolId) {
        this.rolId = rolId;
    }

    public long getRolId() {
        return rolId;
    }

    public void setRolId(int rolId) {
        this.rolId = rolId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
