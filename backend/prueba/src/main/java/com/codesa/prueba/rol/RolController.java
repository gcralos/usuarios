package com.codesa.prueba.rol;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/api/v1")
public class  RolController {

    @Autowired
    RolServices  rolServices;
    private ModelMapper modelMapper = new ModelMapper();


    @GetMapping("/roles")
    public List<RolEntity> getRol(){
        
        RolDTO  rolDTO  = modelMapper.map(rolServices.get(),RolDTO.class);
        return rolServices.get();
    }


}
