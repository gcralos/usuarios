package com.codesa.prueba.rol;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
interface RolRepository  extends JpaRepository<RolEntity,Long > {

    RolEntity findByRolId(Long aLong);
}
