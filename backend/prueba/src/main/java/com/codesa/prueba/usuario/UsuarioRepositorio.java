package com.codesa.prueba.usuario;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface UsuarioRepositorio  extends JpaRepository<UsuarioEntity,Long> {
    UsuarioEntity findAllByIdUsuario( long  id );
}
