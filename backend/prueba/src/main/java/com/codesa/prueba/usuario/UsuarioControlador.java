package com.codesa.prueba.usuario;

import com.codesa.prueba.rol.RolEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/api/v1")
public class UsuarioControlador {

    @Autowired
    private UsuarioServices usuarioServices;

    private ModelMapper modelMapper = new ModelMapper();

    @GetMapping("/usuarios")
    private List<UsuarioEntity> getUsers(){

        return usuarioServices.findAll();
    }


    @PostMapping("/usuario")
    private ResponseEntity<Object> addProFile(@RequestBody UsuarioDTO usuarioDTO ){


        RolEntity  rolEntity = modelMapper.map(usuarioDTO.getRolDTO(),RolEntity.class);
        UsuarioEntity  usuarioEntity = modelMapper.map(usuarioDTO, UsuarioEntity.class );
        usuarioEntity.setRolEntity(rolEntity);
        return usuarioServices.add(usuarioEntity);
    }

    @PutMapping("/usuario/{usuarioId}")
    private  ResponseEntity<Object>  updateProfile( @RequestBody UsuarioDTO usuarioDTO, @PathVariable   int usuarioId){
        RolEntity  rolEntity = modelMapper.map(usuarioDTO.getRolDTO(),RolEntity.class);
        UsuarioEntity  usuarioEntity = modelMapper.map(usuarioDTO,UsuarioEntity.class);
        usuarioEntity.setRolEntity(rolEntity);
        return  usuarioServices.update(usuarioEntity,usuarioId);
    }

    @DeleteMapping("/usuario/{usuarioid}")
    private  void   delete( @PathVariable  long usuarioid){
        System.out.println(usuarioid);
        usuarioServices.delete(usuarioid);
    }

}
