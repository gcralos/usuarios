package com.codesa.prueba.rol;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RolServices {
     @Autowired
     RolRepository rolRepository;

     public List<RolEntity> get(){
         return  rolRepository.findAll();
     }
}
