package com.codesa.prueba.usuario;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UsuarioServices {

    @Autowired
    UsuarioRepositorio usuarioRepositorio;

    public List<UsuarioEntity> findAll(){
        return  usuarioRepositorio.findAll();
    }

    public  UsuarioEntity findById( long id){
        Optional<UsuarioEntity> optional= usuarioRepositorio.findById(id);
        return optional.get();
    }
    public ResponseEntity<Object> add(UsuarioEntity usuario){
        UsuarioEntity usuarioEntity = usuarioRepositorio.save(usuario);
        return  ResponseEntity.status(201).body("usuario  ingrasado  con exito");
    }

    public  ResponseEntity<Object>  update(UsuarioEntity  usuarioEntity, long  id){
        Optional<UsuarioEntity> optional = usuarioRepositorio.findById(id);
        usuarioEntity.setIdUsuario(id);
        usuarioRepositorio.save(usuarioEntity);
        return  ResponseEntity.status(200).body("Perfil  actualizado con exito");
    }

    public  void  delete(long id){
        usuarioRepositorio.deleteById(id);
    }

}
