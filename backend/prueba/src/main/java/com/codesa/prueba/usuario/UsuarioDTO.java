package com.codesa.prueba.usuario;

import com.codesa.prueba.rol.RolDTO;
import com.codesa.prueba.rol.RolEntity;

import javax.persistence.*;

public class UsuarioDTO {


    private long  idUsuario;
    private String nombre;
    private RolDTO rolDTO;
    private  boolean active;

    public long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public RolDTO getRolDTO() {
        return rolDTO;
    }

    public void setRolDTO(RolDTO rolDTO) {
        this.rolDTO = rolDTO;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
