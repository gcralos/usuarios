package com.codesa.prueba.rol;

import java.io.Serializable;
import java.util.List;

public class RolDTO implements Serializable {
    private long  rolId;
    private  String  nombre;

    public long getRolId() {
        return rolId;
    }

    public void setRolId(long rolId) {
        this.rolId = rolId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
