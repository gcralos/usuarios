import {RolModel} from './RolModel';
export  interface UsuariosModel {
  idUsuario: number;
  nombre: string;
  rolDTO: RolModel;
  active: boolean;
}
