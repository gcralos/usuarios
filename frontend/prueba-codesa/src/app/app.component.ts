import {Component, OnInit} from '@angular/core';
import { UsuariosRolService } from './servicios/usuarios-rol.service';
import {RolModel} from './model/RolModel';
import {UsuariosModel} from './model/UsuariosModel';
import {FormBuilder, FormGroup} from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'prueba-codesa';
  rolModel: RolModel[];
  usuariosModel: UsuariosModel[];
  formGroupUsuario: FormGroup;
  resul: any;
  acciones: string;

  constructor( private usuarioservicesrol: UsuariosRolService,
               private formBuilderUsuario: FormBuilder
  ) {
    this.formUsuario();
    this.acciones = 'GUARDAR';
  }
  ngOnInit(): void {

    this.usuarioservicesrol.getRoles().subscribe( res => {
      this.rolModel = res;
    });
    this.usuarioservicesrol.getUsuarios().subscribe(res => {
        this.usuariosModel =  res;
    });
  }

  editarUsuarios( usuariosModel: UsuariosModel){
    this.acciones =  'EDITAR';
    this.setFormUsuario(usuariosModel);
  }
  formUsuario(){
    return    this.formGroupUsuario =  this.formBuilderUsuario.group({
      idUsuario: [''],
      nombre: [''],
      rolDTO: ['1'],
      active: ['']
    });
  }
  setFormUsuario(usuario: any){
    this.formGroupUsuario.get('idUsuario').setValue(usuario.idUsuario);
    this.formGroupUsuario.get('nombre').setValue(usuario.nombre);
    this.formGroupUsuario.get('rolDTO').setValue(usuario.rolDTO);
    this.formGroupUsuario.get('active').setValue(usuario.active);

  }

  guardarUsuario(){
    this.formGroupUsuario.get('rolDTO').setValue(this.setRolUser());
    this.usuarioservicesrol.saveUser(this.formGroupUsuario.value)
      .subscribe(res => {
        console.log(res);
    }, error => { console.log(error); });
  }
  editarUser(){
    this.formGroupUsuario.get('rolDTO').setValue(this.setRolUser());
    this.usuarioservicesrol.updateUsuario(this.formGroupUsuario.value)
      .subscribe(res => {
        console.log(res);
      }, error => {
        console.log(error);
      });
  }
  mensage(){
    Swal.fire({
      position: 'top-end',
      icon: 'success',
      title: 'Your work has been saved',
      showConfirmButton: false,
      timer: 1500
    });

  }
setRolUser(){
   return this.resul  =  this.rolModel.find(function(hero) {
    return hero.rolId == '1';
  });
}

}
