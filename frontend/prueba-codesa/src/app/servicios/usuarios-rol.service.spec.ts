import { TestBed } from '@angular/core/testing';

import { UsuariosRolService } from './usuarios-rol.service';

describe('UsuariosRolService', () => {
  let service: UsuariosRolService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UsuariosRolService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
