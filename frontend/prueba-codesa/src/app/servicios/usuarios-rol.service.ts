import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse} from '@angular/common/http';
import { tap, map, catchError} from 'rxjs/operators';
import {Observable, of, throwError} from 'rxjs';
import {UsuariosModel} from '../model/UsuariosModel';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class UsuariosRolService {
  API_URL = 'http://localhost:9090/';

  constructor(private http: HttpClient) {
  }

  getRoles(): Observable<any> {
    return this.http.get<any>(this.API_URL + 'api/v1/roles')
      .pipe(map(resp => resp),
        catchError(this.handleError));
  }

  getUsuarios(): Observable<any> {
    return this.http.get<any>(this.API_URL + 'api/v1/usuarios')
      .pipe( map( resp => resp),
        catchError(this.handleError));
  }

  saveUser(usuario: UsuariosModel): Observable<any> {
    return this.http.post<any>(this.API_URL + 'api/v1/usuario', usuario);
  }
  updateUsuario(usuario: UsuariosModel): Observable<any> {

    return this.http.put<any>(this.API_URL + 'api/v1/usuario/' + usuario.idUsuario, usuario);
  }
  deleteAll(idUser): Observable<any> {
    console.log(idUser);
    return this.http.delete(this.API_URL + 'api/v1/usuario/' + idUser);
  }

  handleError(error: HttpErrorResponse) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...'
      });
      return  throwError(error.message);
    }
  }



