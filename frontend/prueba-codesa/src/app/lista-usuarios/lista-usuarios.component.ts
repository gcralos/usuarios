import {Component, OnInit, AfterViewInit, ViewChild, Input, Output, EventEmitter, } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {UsuariosModel} from '../model/UsuariosModel';
import {UsuariosRolService} from '../servicios/usuarios-rol.service';

@Component({
  selector: 'app-lista-usuarios',
  templateUrl: './lista-usuarios.component.html',
  styleUrls: ['./lista-usuarios.component.scss']
})
export class ListaUsuariosComponent implements OnInit, AfterViewInit {

  @Input() usuariosInput: UsuariosModel;
  @Output() usuarioOutput: EventEmitter<any> = new EventEmitter();
  usuariosModels: UsuariosModel[];
  dataSource: MatTableDataSource<UsuariosModel>;
  constructor( private usuariosRolServices: UsuariosRolService) {

  }

  displayedColumns: string[] = ['id', 'nombre', 'rol', 'estado', 'aciones'];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
  ngOnInit(): void {
    this.getUsuarios();
  }
  getUsuarios(){
    this.usuariosRolServices.getUsuarios().subscribe(res => {
      this.usuariosModels = res;
      console.log(res);
      this.dataSource = new MatTableDataSource<UsuariosModel>(this.usuariosModels);

    });
  }
   eliminar(usuario: any){
      this.usuariosRolServices.deleteAll(usuario.idUsuario).subscribe(res => {
         this.getUsuarios();
      },
        error => {
         console.log(error);
        });
   }
   editar( usuarios: UsuariosModel){
    const acion = 'editar';
    this.usuarioOutput.emit(usuarios);
   }
}

